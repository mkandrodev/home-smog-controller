unsigned long myChannelNumber = 1284763; //your_thingspeak_channel_number
const String myWriteAPIKey = "LABYYRA7KCXK8NTZ"; //your_thigspeak_API_key
const char* server = "api.thingspeak.com";

char ssid[] = "DREAMIT"; //your_wifi_name
char pass[] = "Zgadnij3006saM"; //your_wifi_password

#define TS_DELAY 60 * 1000
#include <SoftwareSerial.h>

#include <ESP8266WiFi.h>
#include "bme_sensor.h"
#include <SoftwareSerial.h>

int RELAY_PIN = 10;

/*****************************************************************
  /* convert float to string with a                                *
  /* precision of two decimal places                               *
  /*****************************************************************/
String Float2String(const float value)
{
  // Convert a float to String with two decimals.
  char temp[15];
  String s;

  dtostrf(value, 13, 2, temp);
  s = String(temp);
  s.trim();
  return s;
}

WiFiClient client;

void setup() {}

void loop() {
  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, LOW);
  Serial.begin(115200);
  WiFi.persistent(false);
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  delay(10);

  setupBme();
  delay(2000);
  // sometimes parallel sending data and web page will stop nodemcu, watchdogtimer set to 30 seconds
  wdt_disable();
  wdt_enable(30000); // 30 sec

  readBme();

  if (client.connect(server, 80)) {
    Serial.println("Client connected!");
    String upadteData = "api_key=" + myWriteAPIKey;

    upadteData += "&field3=";
    upadteData += Float2String(float(bmeData.temperature));
    upadteData += "&field4=";
    upadteData += Float2String(float(bmeData.humidity));
    upadteData += "\r\n\r\n";

    Serial.println("wywołanie: " + upadteData);

    client.print("GET /update?" + upadteData + "HTTP/1.1");
    client.print(String(server));
    client.println("Host: " + String(server));
    client.println("Connection: close");
    client.println();

    Serial.println("%. Send to Thingspeak.");
  } else {
    Serial.println("Connection failed");
  }
  client.stop();

  digitalWrite(RELAY_PIN, HIGH);

  delay(20 * 60 * 1000);
}
